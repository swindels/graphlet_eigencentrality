# graphlet_eigencentrality data


# 'data/annotations/cancer drivers' 

- contains the list of cancer drivers used

# 'data/annotations/reactome_2_genes/'

- Each file in this directory contains Reactome pathway annotations applicable for given network.
- For instance, data/annotations/reactome_2_genes/PPI_human_Reactome.tsv assignes genes in the human PPI network to Reactome pathways.
- Lines are tab-delimited, with the first entry corresponding to the Reactome pathway identifier and the rest of the entries corresponding to NCBI gene id's.
- Genes that have no interaction in the network are not included in this annotation file.

# 'data/networks/'

- Contains the molecular networks used in the paper in edgelist format.
- Each row corresponds to an each, in which the two entries correspond to two genes (NCBI entrez IDs) interacting.






